<img src="https://user-content.gitlab-static.net/e7d2f3a1f5b517699db50009f3a59ca6dc9308a0/68747470733a2f2f7261772e67697468756275736572636f6e74656e742e636f6d2f767574726174656e6b6f2f7069672f6d61696e2f7069672e706e67" alt="PIG" width="200"/>

# PIG - a dummy app

### Build/Test/Run

To build project hit:

`go build`

To run tests hit:

`go test ./...`

To run the app hit:

`go run main.go`



or run compiled binary



`./pig`


### Notes

Be also aware of ./resources directory which contains index HTML page. This should be copied alongside with ./pig binary as well with the same structure
